<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'leicaaka');

/** MySQL database username */
define('DB_USER', 'homestead');

/** MySQL database password */
define('DB_PASSWORD', 'secret');

/** MySQL hostname */
define('DB_HOST', 'homestead');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'tomrpat8u5ijjd5tnexcbdsj9hx1tfuohasf98nj11u2s6zf5ed1meyxgg8hpr7p');
define('SECURE_AUTH_KEY',  '4i6g2mfhltmzvs5we8lblbknlcdgs2y5npnixpo4pgz3s0qsptjrpgst0fxavo0h');
define('LOGGED_IN_KEY',    'l9moplkbjukybn7v4scehfzdgp2ef636fqjnnd6zyuby52wx268vk2nwbeuh6tbb');
define('NONCE_KEY',        'zb0ywrkafsyw29lohrehxof0z20vbgqrmgij2ipkmaevimq6kdedwff2dq3qbdio');
define('AUTH_SALT',        '4sgos6xwchnwcscqtx1cfwpuz6ixqps70vyibcd8hdutcrvjiy8nfz5vm2upc75y');
define('SECURE_AUTH_SALT', 'urwenui6pn5q4y3vrqvrjstp8yrg2f9icbigpoj2y9wz2uf5zbvb0t6uoypaqbsl');
define('LOGGED_IN_SALT',   'czey3bexcsz9pvhmh91gbwbggif09ji1f6fakywocujikevrejtdk4w4abzcphrr');
define('NONCE_SALT',       'eigxb83cl3bve8cyacvzk6zw16tkt1g1bzhueuxpntq9prhjz3zka6beqtr2xnwt');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'xywp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
